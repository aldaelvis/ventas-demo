# Sistema de Ventas en JAKARTA EE

Tecnologias usadas : 
1. Jax RS
2. JPA
3. Eclipse Microprofile JWT
4. Payara Server 5

Para consumir la API se necesita authenticacion por token ( JWT )
y el dependiendo a los roles se le da acceso.
---
Un JWT tambi�n debe estar firmado y, en el caso de MicroProfile JWT, 
debe ser RSASSA-PKCS-v1_5 utilizando el algoritmo hash SHA-256:

Una forma de hacer esta firma es generando primero un par de claves SSH de la siguiente manera:

1. Abrir terminal
2. Generar la llave base ingresando: ``` openssl genrsa -out baseKey.pem ```
3. A partir de la clave base, genere la clave privada PKCS # 8: ``` openssl pkcs8 -topk8 -inform PEM -in baseKey.pem -out privateKey.pem -nocrypt ```
3. Y generar la llave publica: ``` openssl rsa -in baseKey.pem -pubout -outform PEM -out publicKey.pem ```

Listado de roles :
1. Rol almacenero (categoria, articulos, ingresos)
2. Rol admin ( All )
3. Rol vendedor ( Ventas )

Ejemplos
## Categorias

-------
- Insertar nueva categoria ( POST )

``` http request
http://localhost:8080/api/categoria
```

``` json
{
    "nombre": "Celulares",
    "descripcion": "Todo tipo de telefono"
}
```


## Articulos

---
Insertar nuevo producto

``` json
{
    "nombre": "iPhone 6s",
    "descripcion": "Color plateado",
    "precio": 1500.00,
    "stock": 1,
    "categoria": {
        "id": 1
    }
}
```
