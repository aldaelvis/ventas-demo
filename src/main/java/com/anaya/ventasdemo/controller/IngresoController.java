package com.anaya.ventasdemo.controller;

import com.anaya.ventasdemo.dto.IngresoDto;
import com.anaya.ventasdemo.model.DetallesIngreso;
import com.anaya.ventasdemo.model.Ingreso;
import com.anaya.ventasdemo.model.Usuario;
import com.anaya.ventasdemo.repository.IngresoRepository;
import com.anaya.ventasdemo.repository.UsuarioRepository;
import jakarta.annotation.security.RolesAllowed;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.eclipse.microprofile.jwt.Claim;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Path("/ingreso")
@RolesAllowed({"ROLE_ADMIN", "ROLE_ALMACENERO"})
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class IngresoController {

    @Inject
    @Claim("id")
    private Long id;

    @Inject
    private UsuarioRepository usuarioRepository;

    @Inject
    private IngresoRepository ingresoRepository;

    @GET
    public Response listarIngresos() {
        List<Ingreso> ingresos = ingresoRepository.getIngresos();
        List<IngresoDto> ingresoDtos = new ArrayList<>();

        if(ingresos.size() > 0) {
            for (Ingreso ingreso : ingresos) {
                var ingresoDto = new IngresoDto();
                ingresoDto.setId(ingreso.getId());
                ingresoDto.setFecha(ingreso.getFecha());
                ingresoDto.setImpuesto(ingreso.getImpuesto());
                ingresoDto.setPersona(ingreso.getPersona());
                ingresoDto.setUsuario(ingreso.getUsuario());
                ingresoDto.setNumComprobante(ingreso.getNumComprobante());
                ingresoDto.setNumComprobante(ingreso.getSerieComprobante());
                ingresoDto.setTipoDocumento(ingreso.getTipoDocumento());
                ingresoDto.setTotal(ingreso.getTotal());
                ingresoDtos.add(ingresoDto);
            }
        }

        return Response.ok(ingresoDtos).build();
    }

    @GET
    @Path("/{id}")
    public Response obtenerIngreso(@PathParam("id") Long id) {

        Ingreso ingreso = ingresoRepository.findById(id);
        if (ingreso == null) {
            Map<String, String> messages = new HashMap<>();
            messages.put("message", "No se encontro el ingreso");
            return Response
                    .status(Response.Status.NOT_FOUND)
                    .entity(messages)
                    .build();
        }

        return Response.ok(ingreso).build();
    }

    @POST
    public Response guardarIngreso(Ingreso ingreso) {

        Usuario usuario = usuarioRepository.findById(id);
        if (usuario == null) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        // Paso el filtro - guardado
        ingreso.setUsuario(usuario);
         ingresoRepository.store(ingreso);

        return Response.ok("Guardado").build();
    }

    @GET
    @Path("/{id}/detalles")
    public Response obtenerDetallesIngreso(@PathParam("id") Long id) {
        List<DetallesIngreso> detallesIngresos = ingresoRepository.getDetallesIngresos(id);

        return Response.ok(detallesIngresos).build();
    }

    @GET
    @Path("/{id}/cancel")
    public Response cancelarIngreso(@PathParam("id") Long id) {
        ingresoRepository.statusCancel(id);
        Map<String, String> messages = new HashMap<>();
        messages.put("message", "Ingreso cancelado correctamente");

        return Response.ok(messages).build();
    }
}
