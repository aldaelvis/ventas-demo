package com.anaya.ventasdemo.controller;

import com.anaya.ventasdemo.model.Articulo;
import com.anaya.ventasdemo.repository.ArticuloRepository;
import jakarta.annotation.security.DeclareRoles;
import jakarta.annotation.security.RolesAllowed;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriBuilder;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Path("/articulo")
public class ArticuloController {

    @Inject
    private ArticuloRepository articuloRepository;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed({"ROLE_ADMIN", "ROLE_ALMACENERO", "ROLE_VENDEDOR"})
    public List<Articulo> obtenerArticulos() {
        return articuloRepository.getArticulos();
    }

    @GET
    @Path("/{id}")
    @RolesAllowed({"ROLE_ADMIN", "ROLE_ALMACENERO", "ROLE_VENDEDOR"})
    public Response obtenerArticulo(@PathParam("id") Long id) {
        Articulo articulo = articuloRepository.findById(id);

        if (articulo == null) {
            Map<String, String> message = new HashMap<>();
            message.put("message", "Articulo no encontrado");
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(message)
                    .build();
        }

        return Response.ok(articulo).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({"ROLE_ADMIN", "ROLE_ALMACENERO"})
    public Response guardarArticulo(Articulo articulo) {
        articuloRepository.store(articulo);
        URI location = UriBuilder.fromResource(CategoriaController.class)
                .path("/{id}")
                .resolveTemplate("id", articulo.getCategoria().getId())
                .build();

        return Response.created(location).build();

    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({"ROLE_ADMIN", "ROLE_ALMACENERO"})
    public Response editarArticulo(@PathParam("id") Long id, Articulo articulo) {
        if (!Objects.equals(id, articulo.getId())) {
            throw new BadRequestException("El id debe de coincidir con el parametro de ruta");
        }

        articuloRepository.update(articulo);

        return Response.ok(articulo).build();
    }

    @DELETE
    @Path("/{id}")
    @RolesAllowed({"ROLE_ADMIN", "ROLE_ALMACENERO"})
    public Response eliminarArticulo(@PathParam("id") Long id) {
        Map<String, String> messages = new HashMap<>();
        if (articuloRepository.findById(id) == null) {
            messages.put("message", "No existe ningun articulo con el id: " + id);

            return Response.status(Response.Status.NOT_FOUND)
                    .entity(messages)
                    .build();
        }
        messages.put("message", "Se elimino correctamente el articulo");
        articuloRepository.delete(id);

        return Response.ok(messages).build();
    }
}
