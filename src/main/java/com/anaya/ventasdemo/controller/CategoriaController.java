package com.anaya.ventasdemo.controller;

import com.anaya.ventasdemo.jpa.JpaUsuario;
import com.anaya.ventasdemo.model.Categoria;
import com.anaya.ventasdemo.repository.CategoriaRepository;
import jakarta.annotation.security.RolesAllowed;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.eclipse.microprofile.jwt.Claim;
import org.eclipse.microprofile.jwt.JsonWebToken;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

@Path("/categoria")
@RolesAllowed({"ROLE_ADMIN", "ROLE_ALMACENERO"})
public class CategoriaController {

    @Inject
    private CategoriaRepository categoriaRepository;

    @Inject
    private JsonWebToken jsonWebToken;

    @Inject
    @Claim("id")
    private Long id;

    public static final Logger logger =
            Logger.getLogger(JpaUsuario.class.getCanonicalName());

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response obtenerCategorias() {
        List<Categoria> categorias = categoriaRepository.getCategorias();
        String nombre = jsonWebToken.getName();

        logger.log(
                Level.INFO, "User[ Nombre:" + nombre + ", id: " + id + " ]");
        return Response.ok(categorias).build();
    }

    @GET
    @Path("/{id}")
    public Response buscarCategoria(@PathParam("id") Long id) {

        Categoria categoria = categoriaRepository.findById(id);
        Map<String, String> messages = new HashMap<>();
        if (categoria == null) {
            messages.put("message", "No se encontro la categoria");
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(messages)
                    .build();
        }

        return Response.ok(categoriaRepository.findById(id)).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response guardarCategoria(Categoria categoria) {
        categoriaRepository.store(categoria);
        Map<String, String> response = new HashMap<>();
        response.put("message", "Creado correctamente");

        return Response.ok(response).build();
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response editarCategoria(@PathParam("id") Long id, Categoria categoria) {
        Map<String, Object> response = new HashMap<>();
        if (categoriaRepository.findById(categoria.getId()) == null) {
            response.put("message", "No se encontro la categoria");

            return Response.status(Response.Status.NOT_FOUND)
                    .entity(response)
                    .build();
        }

        categoriaRepository.update(categoria);
        response.put("message", "Categoria actualizado correctamente");
        response.put("categoria", categoria);

        return Response.ok(response).build();
    }

    @DELETE
    @Path("/{id}")
    public Response eliminarCategoria(@PathParam("id") Long id) {
        Map<String, Object> response = new HashMap<>();
        Categoria categoria = categoriaRepository.findById(id);
        if (categoria == null) {
            response.put("message", "No se puede eliminar, no se encontro la categoria");

            return Response.status(Response.Status.NOT_FOUND)
                    .entity(response)
                    .build();
        }

        categoriaRepository.delete(id);
        response.put("message", "Se elimino la categoria");

        return Response.ok(response).build();
    }

}
