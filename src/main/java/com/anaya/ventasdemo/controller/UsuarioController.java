package com.anaya.ventasdemo.controller;

import com.anaya.ventasdemo.model.Usuario;
import com.anaya.ventasdemo.repository.UsuarioRepository;
import com.anaya.ventasdemo.security.ChyperService;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.security.RolesAllowed;
import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Inject;
import jakarta.json.Json;
import jakarta.json.JsonObject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

@RequestScoped
@Path("/usuario")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class UsuarioController {

    @Inject
    private ChyperService chyperService;

    @Inject
    private UsuarioRepository usuarioRepository;

    public static final Logger logger =
            Logger.getLogger(UsuarioController.class.getCanonicalName());

    PrivateKey privateKey;

    @PostConstruct
    public void init() {
        try {
            privateKey = chyperService.readPrivateKey("privateKey.pem");
        } catch (IOException | NoSuchAlgorithmException | InvalidKeySpecException e) {
            e.printStackTrace();
        }
    }

    @POST
    @RolesAllowed({"ROLE_ADMIN"})
    public Response guardarUsuario(Usuario usuario) {

        usuarioRepository.store(usuario);
        Map<String, String> messages = new HashMap<>();
        messages.put("message", "Usuario guardado correctamente");

        return Response.ok(messages).build();
    }

    @POST
    @Path("/login")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response login(@FormParam("email") String email, @FormParam("password") String password) {
        Usuario usuario = usuarioRepository.login(email, password);
        // logger.log(Level.INFO, email + " " +password + " User: ", usuario );

        if (usuario != null) {
            Map<String, Object> messages = new HashMap<>();
            JsonObject jsonObject = Json.createObjectBuilder()
                    .add("id", usuario.getId())
                    .add("email", usuario.getEmail())
                    .add("nombre", usuario.getNombre())
                    .build();

            String token = chyperService.generateJwtString(privateKey, usuario.getId(), usuario.getEmail(), usuario.getRoles());
            messages.put("token", "Bearer ".concat(token));
            messages.put("usuario", jsonObject);
            return Response.ok(messages).build();
        }

        return Response.status(Response.Status.FORBIDDEN)
                .build();
    }
}
