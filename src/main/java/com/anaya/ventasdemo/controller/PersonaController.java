package com.anaya.ventasdemo.controller;

import com.anaya.ventasdemo.model.Persona;
import com.anaya.ventasdemo.repository.PersonaRepository;
import jakarta.annotation.security.RolesAllowed;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Path("/persona")
@RolesAllowed({"ROLE_ADMIN", "ROLE_ALMACENERO", "ROLE_VENDEDOR"})
public class PersonaController {

    @Inject
    private PersonaRepository personaRepository;


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarPersonas() {

        List<Persona> personas = personaRepository.getPersonas();

        return Response.ok(personas).build();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response obtenerPersona(@PathParam("id") Long id) {
        Persona persona = personaRepository.findById(id);
        if (persona == null) {
            Map<String, String> messages = new HashMap<>();
            messages.put("message", "No se encontro la persona");
            return Response
                    .status(Response.Status.NOT_FOUND)
                    .entity(messages)
                    .build();
        }

        return Response.ok(persona).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response guardarPersona(Persona persona) {
        personaRepository.store(persona);
        Map<String, String> messages = new HashMap<>();
        messages.put("message", "Persona creado correctamente");

        return Response.ok(messages).build();
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    public Response editarPersona(@PathParam("id") Long id, Persona persona) {
        Map<String, Object> messages = new HashMap<>();
        if (!Objects.equals(id, persona.getId())) {
            messages.put("message", "El Id debe de coincidir con el parametro de ruta");
            throw new BadRequestException("El id debe de coincidir con el parametro de ruta");
        }

        personaRepository.update(persona);
        messages.put("message", "Persona actualizada correctamente");
        messages.put("persona", persona);

        return Response.ok(messages).build();
    }

    @DELETE
    @Path("/{id}")
    public Response eliminarPersona(@PathParam("id") Long id) {
        Map<String, Object> messages = new HashMap<>();
        personaRepository.delete(id);

        messages.put("message", "Persona eliminada correctamente");

        return Response.ok(messages).build();
    }

}
