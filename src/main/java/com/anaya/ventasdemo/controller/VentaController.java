package com.anaya.ventasdemo.controller;

import com.anaya.ventasdemo.dto.VentaDto;
import com.anaya.ventasdemo.model.DetallesVenta;
import com.anaya.ventasdemo.model.Usuario;
import com.anaya.ventasdemo.model.Venta;
import com.anaya.ventasdemo.repository.UsuarioRepository;
import com.anaya.ventasdemo.repository.VentaRepository;
import jakarta.annotation.security.RolesAllowed;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.eclipse.microprofile.jwt.Claim;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Path("/venta")
@RolesAllowed({"ROLE_ADMIN", "ROLE_VENDEDOR"})
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class VentaController {

    @Inject
    @Claim("id")
    private Long authenticatedUserId;

    @Inject
    private UsuarioRepository usuarioRepository;

    @Inject
    private VentaRepository ventaRepository;

    @GET
    public Response obtenerVentas() {
        List<Venta> ventas = ventaRepository.getVentas();
        List<VentaDto> ventaDtos = new ArrayList<>();

        if (ventas.size() > 0) {
            for (Venta venta : ventas) {
                var ventaDto = new VentaDto();
                ventaDto.setId(venta.getId());
                ventaDto.setFecha(venta.getFecha());
                ventaDto.setImpuesto(venta.getImpuesto());
                ventaDto.setCliente(venta.getCliente());
                ventaDto.setUsuario(venta.getUsuario());
                ventaDto.setNumComprobante(venta.getNumComprobante());
                ventaDto.setNumComprobante(venta.getSerieComprobante());
                ventaDto.setTipoComprobante(venta.getTipoComprobante());
                ventaDto.setTotal(venta.getTotal());
                ventaDtos.add(ventaDto);
            }
        }

        return Response.ok(ventaDtos).build();
    }

    @GET
    @Path("/{id}")
    public Response obtenerVenta(@PathParam("id") Long id) {

        Venta venta = ventaRepository.findById(id);
        if (venta == null) {
            Map<String, String> messages = new HashMap<>();
            messages.put("message", "No se encontro la venta");
            return Response
                    .status(Response.Status.NOT_FOUND)
                    .entity(messages)
                    .build();
        }

        return Response.ok(venta).build();
    }


    @POST
    public Response guardarVenta(Venta venta) {

        Usuario usuario = usuarioRepository.findById(authenticatedUserId);
        if (usuario == null) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        // Paso el filtro - guardado
        venta.setUsuario(usuario);
        // List<Map<Long, String>> rpta = ventaRepository.store(venta);
        Map<Long, String> rpta = ventaRepository.store(venta);
        if (rpta.isEmpty()) {
            return Response.ok("Guardado").build();
        } else {
            /*Map<Long, String> messages = new HashMap<>();
            for (Map<Long, String> map : rpta) {
                messages.putAll(map);
            }*/
            return Response.status(Response.Status.BAD_REQUEST).entity(rpta).build();
        }
    }

    @GET
    @Path("/{id}/detalles")
    public Response obtenerDetallesVenta(@PathParam("id") Long id) {
        List<DetallesVenta> detallesVenta = ventaRepository.getDetallesVentas(id);

        return Response.ok(detallesVenta).build();
    }

    @GET
    @Path("/{id}/cancel")
    public Response cancelarVenta(@PathParam("id") Long id) {
        ventaRepository.statusCancel(id);
        Map<String, String> messages = new HashMap<>();
        messages.put("message", "Venta cancelado correctamente");

        return Response.ok(messages).build();
    }

}
