package com.anaya.ventasdemo.jpa;

import com.anaya.ventasdemo.model.Articulo;
import com.anaya.ventasdemo.repository.ArticuloRepository;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;

import java.util.List;

@ApplicationScoped
public class JpaArticulo implements ArticuloRepository {

    @PersistenceContext
    private EntityManager em;

    @Override
    public Articulo findById(Long id) {
        Articulo articulo;
        try {
            articulo = em.find(Articulo.class, id);
        } catch (Exception e) {
            articulo = null;
        }

        return articulo;
    }

    @Override
    public List<Articulo> getArticulos() {
        return em.createNamedQuery("Articulo.findAll", Articulo.class).getResultList();
    }

    @Override
    @Transactional
    public void store(Articulo articulo) {
        em.persist(articulo);
    }

    @Override
    @Transactional
    public void update(Articulo articulo) {
        em.merge(articulo);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        Articulo articulo = em.find(Articulo.class, id);
        em.remove(articulo);
    }
}
