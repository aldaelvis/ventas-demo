package com.anaya.ventasdemo.jpa;

import com.anaya.ventasdemo.model.Persona;
import com.anaya.ventasdemo.repository.PersonaRepository;
import jakarta.persistence.EntityManager;
import jakarta.persistence.NoResultException;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;

import java.util.List;

public class JpaPersona implements PersonaRepository {

    @PersistenceContext
    private EntityManager em;

    @Override
    public Persona findById(Long id) {
        Persona persona;
        try {
            persona = em.find(Persona.class, id);
        } catch (NoResultException e) {
            persona = null;
        }

        return persona;
    }

    @Override
    public List<Persona> getPersonas() {
        return em.createNamedQuery("Persona.findAll", Persona.class).getResultList();
    }

    @Override
    @Transactional
    public void store(Persona persona) {
        em.persist(persona);
    }

    @Override
    @Transactional
    public void update(Persona persona) {
        em.merge(persona);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        Persona persona = em.find(Persona.class, id);
        em.remove(persona);
    }
}
