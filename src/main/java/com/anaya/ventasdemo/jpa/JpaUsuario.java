package com.anaya.ventasdemo.jpa;

import com.anaya.ventasdemo.model.Usuario;
import com.anaya.ventasdemo.repository.UsuarioRepository;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import org.mindrot.jbcrypt.BCrypt;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@ApplicationScoped
public class JpaUsuario implements UsuarioRepository {

    @PersistenceContext
    private EntityManager em;

    public static final Logger logger =
            Logger.getLogger(JpaUsuario.class.getCanonicalName());

    private static int workload = 12;

    @Override
    public Usuario findById(Long id) {
        Usuario usuario;
        try {
            usuario = em.find(Usuario.class, id);
        } catch (Exception e) {
            usuario = null;
        }

        return usuario;
    }

    @Override
    public List<Usuario> getUsuarios() {
        return em.createNamedQuery("Usuario.findAll", Usuario.class).getResultList();
    }

    @Override
    @Transactional
    public void store(Usuario usuario) {
        String salt = BCrypt.gensalt(workload);
        String pass = usuario.getPassword();
        String passHash = BCrypt.hashpw(pass, salt);
        usuario.setPassword(passHash);
        em.persist(usuario);
    }

    @Override
    @Transactional
    public void update(Usuario usuario) {
        em.merge(usuario);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        Usuario usuario = em.find(Usuario.class, id);
        usuario.setEstado(0);
        em.merge(usuario);
    }

    @Override
    public Usuario login(String email, String password) {
        Usuario usuario = em.createQuery("SELECT u FROM Usuario u WHERE u.email = :email", Usuario.class)
                .setParameter("email", email)
                .getSingleResult();
        if (usuario != null) {
            logger.log(
                    Level.INFO, " Password Hash: " + usuario.getPassword());
            if (BCrypt.checkpw(password, usuario.getPassword())) {
                logger.log(
                        Level.INFO, "Password verify ");
                return usuario;
            }
        }
        return null;
    }
}
