package com.anaya.ventasdemo.jpa;

import com.anaya.ventasdemo.model.Articulo;
import com.anaya.ventasdemo.model.DetallesIngreso;
import com.anaya.ventasdemo.model.Ingreso;
import com.anaya.ventasdemo.repository.ArticuloRepository;
import com.anaya.ventasdemo.repository.IngresoRepository;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;

import java.util.List;
import java.util.logging.Logger;

@ApplicationScoped
@Transactional
public class JpaIngreso implements IngresoRepository {

    @PersistenceContext
    private EntityManager em;

    @Inject
    private ArticuloRepository articuloRepository;

    public static final Logger logger =
            Logger.getLogger(JpaUsuario.class.getCanonicalName());

    @Override
    public Ingreso findById(Long id) {
        Ingreso ingreso;
        try {
            ingreso = em.find(Ingreso.class, id);
        } catch (Exception e) {
            ingreso = null;
        }

        return ingreso;
    }

    @Override
    public List<Ingreso> getIngresos() {
        return em.createNamedQuery("Ingreso.findAll", Ingreso.class).getResultList();
    }

    @Override
    public void store(Ingreso ingreso) {
        // Todo refactorizar funcional
        for (DetallesIngreso detIngreso : ingreso.getDetalles()) {
            Articulo articulo = articuloRepository.findById(detIngreso.getArticulo().getId());
            int stock = detIngreso.getCantidad();
            articulo.setStock(stock + articulo.getStock());
            em.merge(articulo);
        }

        em.persist(ingreso);
    }

    @Override
    public void update(Ingreso ingreso) {

    }

    @Override
    public void delete(Long id) {

    }

    @Override
    public void statusCancel(Long id) {

        Ingreso ingreso = findById(id);
        if (ingreso != null) {
            for (DetallesIngreso detalles : ingreso.getDetalles()) {
                int stockDet = detalles.getCantidad();
                Articulo articulo = articuloRepository.findById(detalles.getArticulo().getId());
                articulo.setStock(articulo.getStock() - stockDet);
                em.merge(articulo);
            }

            ingreso.setEstado(0);
        }

        em.merge(ingreso);
    }

    @Override
    public List<DetallesIngreso> getDetallesIngresos(Long id) {

        return em.createNamedQuery("Ingreso.listDetalles", DetallesIngreso.class)
                .setParameter("id", id)
                .getResultList();
    }
}
