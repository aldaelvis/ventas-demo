package com.anaya.ventasdemo.jpa;

import com.anaya.ventasdemo.model.Articulo;
import com.anaya.ventasdemo.model.DetallesVenta;
import com.anaya.ventasdemo.model.Venta;
import com.anaya.ventasdemo.repository.ArticuloRepository;
import com.anaya.ventasdemo.repository.VentaRepository;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ApplicationScoped
@Transactional
public class JpaVenta implements VentaRepository {

    @PersistenceContext
    private EntityManager em;

    @Inject
    private ArticuloRepository articuloRepository;

    @Override
    public Venta findById(Long id) {
        Venta venta;
        try {
            venta = em.find(Venta.class, id);
        } catch (Exception e) {
            venta = null;
        }

        return venta;
    }

    @Override
    public List<Venta> getVentas() {
        return em.createNamedQuery("Venta.findAll", Venta.class)
                .getResultList();
    }

    @Override
    public Map<Long, String> store(Venta venta) {
        Map<Long, String> validacionDetalle = new HashMap<>();

        for (DetallesVenta detalles : venta.getDetallesVentas()) {

            Articulo articulo = articuloRepository.findById(detalles.getArticulo().getId());
            int cantidad = detalles.getCantidad();
            // stocK: 15 >= 1
            if (articulo.getStock() >= cantidad) {
                articulo.setStock(articulo.getStock() - cantidad);
                em.merge(articulo);
            } else {
                validacionDetalle.put(articulo.getId(), "El articulo con ID: " + articulo.getId() + ", no tiene suficiente stock");
                // validaciones.add(validacionDetalle);
            }
        }

        if (validacionDetalle.isEmpty()) {
            em.persist(venta);
        }

        return validacionDetalle;
    }

    @Override
    public void update(Venta ingreso) {

    }

    @Override
    public void delete(Long id) {

    }

    @Override
    public void statusCancel(Long id) {
        Venta venta = findById(id);
        if (venta != null) {
            for (DetallesVenta detalles : venta.getDetallesVentas()) {
                int stockDet = detalles.getCantidad();
                Articulo articulo = articuloRepository.findById(detalles.getArticulo().getId());
                articulo.setStock(articulo.getStock() + stockDet);
                em.merge(articulo);
            }

            venta.setEstado(0);
        }

        em.merge(venta);
    }

    @Override
    public List<DetallesVenta> getDetallesVentas(Long id) {
        return em.createNamedQuery("Venta.listDetalles", DetallesVenta.class)
                .setParameter("id", id)
                .getResultList();
    }
}
