package com.anaya.ventasdemo.jpa;

import com.anaya.ventasdemo.model.Categoria;
import com.anaya.ventasdemo.repository.CategoriaRepository;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.persistence.EntityManager;
import jakarta.persistence.NoResultException;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

@ApplicationScoped
public class JpaCategoria implements CategoriaRepository, Serializable {

    static final Logger LOGGER = Logger.getLogger(JpaCategoria.class.getCanonicalName());

    @PersistenceContext
    private EntityManager em;


    @Override
    public Categoria findById(Long id) {

        Categoria categoria;
        try {
            categoria = em.find(Categoria.class, id);
        } catch (NoResultException e) {
            LOGGER.log(Level.FINE, "error al encontrar categoria", e.getMessage());
            categoria = null;
        }

        return categoria;
    }

    @Override
    public List<Categoria> getCategorias() {
        return em.createNamedQuery("Categoria.findAll", Categoria.class).getResultList();
    }

    @Transactional
    @Override
    public void store(Categoria categoria) {
        Objects.requireNonNull(categoria);
        em.persist(categoria);
    }

    @Override
    @Transactional
    public void update(Categoria categoria) {
        em.merge(categoria);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        Categoria categoria = em.find(Categoria.class, id);
        em.remove(categoria);
    }
}
