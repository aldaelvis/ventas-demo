package com.anaya.ventasdemo.security;

import com.anaya.ventasdemo.model.Rol;
import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.RSASSASigner;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.*;

public class ChyperService {

    public String generateJwtString(PrivateKey key, Long id, String subject, List<Rol> groups) {

        JWSHeader header = new JWSHeader.Builder(JWSAlgorithm.RS256)
                .type(JOSEObjectType.JWT)
                .keyID("anayaKey")
                .build();

        JwtToken token = new JwtToken();

        token.setAud("anaya");
        token.setIss("http://aldaelvis.com");
        token.setJti(UUID.randomUUID().toString());
        token.setId(id);
        token.setSub(subject);
        token.setUpn(subject);

        Date issueDate = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(issueDate);
        calendar.add(Calendar.MINUTE, 10);
        Date expireDate = calendar.getTime();

        token.setIat(issueDate.getTime());
        token.setExp(expireDate.getTime());

        //---
        Set<String> roles = new HashSet<>();
        for (Rol rol : groups) {
            roles.add(rol.getNombre());
        }
        token.setGroups(roles);
        JWSObject jwsObject = new JWSObject(header, new Payload(token.toJSONString()));
        JWSSigner signer = new RSASSASigner(key);

        try {
            jwsObject.sign(signer);
        } catch (JOSEException e) {
            e.printStackTrace();
        }

        return jwsObject.serialize();
    }

    public PrivateKey readPrivateKey(String resourceName) throws NoSuchAlgorithmException, InvalidKeySpecException, IOException {
        byte[] byteBuffer = new byte[16384];
        int length = Thread.currentThread().getContextClassLoader()
                .getResource(resourceName)
                .openStream()
                .read(byteBuffer);

        String key = new String(byteBuffer, 0, length).replaceAll("-----BEGIN (.*)-----", "")
                .replaceAll("-----END (.*)----", "")
                .replaceAll("\r\n", "")
                .replaceAll("\n", "")
                .trim();

        return KeyFactory.getInstance("RSA")
                .generatePrivate(new PKCS8EncodedKeySpec(Base64.getDecoder().decode(key)));
    }


    public String getString(InputStream is) throws IOException {
        int ch;
        StringBuilder sb = new StringBuilder();
        while ((ch = is.read()) != -1)
            sb.append((char) ch);
        return sb.toString();
    }


}
