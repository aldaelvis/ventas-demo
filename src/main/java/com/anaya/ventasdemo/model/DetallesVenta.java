package com.anaya.ventasdemo.model;

import jakarta.persistence.*;

import java.io.Serializable;

@Entity
@Table(name = "detalle_venta")
public class DetallesVenta implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    private Articulo articulo;

    private int cantidad;

    @Column(precision = 11, scale = 2)
    private double descuento;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Articulo getArticulo() {
        return articulo;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getDescuento() {
        return descuento;
    }

    public void setDescuento(double descuento) {
        this.descuento = descuento;
    }

    public double calcularImporte() {
        return this.cantidad * this.articulo.getPrecio();
    }
}
