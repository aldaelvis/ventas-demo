package com.anaya.ventasdemo.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;

@Entity
@Table(name = "articulos")
@NamedQuery(
        name = "Articulo.findAll",
        query = "SELECT a FROM Articulo a JOIN FETCH a.categoria c ORDER BY a.id DESC"
)
public class Articulo {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull(message = "No puede ser nulo")
    @NotEmpty(message = "No puede estar vacio")
    private String nombre;

    private String descripcion;

    @NotNull(message = "No puede ser nulo")
    private double precio;

    @Min(value = 0)
    private int stock;

    private String urlImage;

    @ManyToOne(fetch = FetchType.LAZY)
    private Categoria categoria;

    public Articulo() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }
}
