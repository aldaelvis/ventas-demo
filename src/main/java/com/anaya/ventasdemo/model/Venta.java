package com.anaya.ventasdemo.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "venta")
@NamedQueries({
        @NamedQuery(name = "Venta.findAll", query = "SELECT v FROM Venta v"),
        @NamedQuery(name = "Venta.listDetalles", query = "SELECT dv FROM Venta v JOIN v.detallesVentas dv  WHERE v.id = :id ")
})
public class Venta implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @NotEmpty
    private String tipoComprobante;

    @NotNull
    @NotEmpty
    private String serieComprobante;

    @NotNull
    @NotEmpty
    private String numComprobante;

    @Temporal(TemporalType.DATE)
    private Date fecha;

    @NotNull
    @Column(precision = 10, scale = 2)
    private double impuesto;

    @Column(precision = 10, scale = 2)
    private double total;

    private int estado;

    @NotNull
    @ManyToOne
    private Usuario usuario;

    @ManyToOne
    private Persona cliente;

    //-- venta|detalles
    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    @JoinColumn(name = "VENTA_ID")
    private List<DetallesVenta> detallesVentas;

    @PrePersist
    public void init() {
        this.fecha = new Date();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTipoComprobante() {
        return tipoComprobante;
    }

    public void setTipoComprobante(String tipoComprobante) {
        this.tipoComprobante = tipoComprobante;
    }

    public String getSerieComprobante() {
        return serieComprobante;
    }

    public void setSerieComprobante(String serieComprobante) {
        this.serieComprobante = serieComprobante;
    }

    public String getNumComprobante() {
        return numComprobante;
    }

    public void setNumComprobante(String numComprobante) {
        this.numComprobante = numComprobante;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public double getImpuesto() {
        return impuesto;
    }

    public void setImpuesto(double impuesto) {
        this.impuesto = impuesto;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public String getUsuario() {
        return usuario.getNombre();
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getCliente() {
        return cliente.getNombre();
    }

    public void setCliente(Persona cliente) {
        this.cliente = cliente;
    }

     public List<DetallesVenta> getDetallesVentas() {
        return detallesVentas;
    }

    public void setDetallesVentas(List<DetallesVenta> detallesVentas) {
        this.detallesVentas = detallesVentas;
    }
}
