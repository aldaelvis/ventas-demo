package com.anaya.ventasdemo.model;

import jakarta.persistence.*;

@Entity
@Table(name = "detalle_ingreso")
public class DetallesIngreso {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    private Articulo articulo;

    private int cantidad;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Articulo getArticulo() {
        return articulo;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double calcularImporte() {
        return this.cantidad * this.articulo.getPrecio();
    }
}
