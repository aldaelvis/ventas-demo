package com.anaya.ventasdemo.model;

import jakarta.persistence.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "ingreso")
@NamedQueries({
        @NamedQuery(name = "Ingreso.findAll", query = "SELECT i FROM Ingreso i"),
        @NamedQuery(name = "Ingreso.listDetalles", query = "SELECT di FROM Ingreso i JOIN i.detallesIngreso di  WHERE i.id = :id ")
})
public class Ingreso implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String tipoDocumento;

    private String serieComprobante;

    private String numComprobante;

    @Temporal(TemporalType.DATE)
    private Date fecha;

    @Column(precision = 10, scale = 2)
    private double impuesto;

    @Column(precision = 10, scale = 2)
    private double total;

    private int estado;

    @ManyToOne
    private Usuario usuario;

    @ManyToOne
    private Persona persona;

    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    @JoinColumn(name = "INGRESO_ID")
    private List<DetallesIngreso> detallesIngreso;

    @PrePersist
    public void init() {
        fecha = new Date();
    }

    public Ingreso() {
        detallesIngreso = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getSerieComprobante() {
        return serieComprobante;
    }

    public void setSerieComprobante(String serieComprobante) {
        this.serieComprobante = serieComprobante;
    }

    public String getNumComprobante() {
        return numComprobante;
    }

    public void setNumComprobante(String numComprobante) {
        this.numComprobante = numComprobante;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public double getImpuesto() {
        return impuesto;
    }

    public void setImpuesto(double impuesto) {
        this.impuesto = impuesto;
    }

    public double getTotal() {
        double total = 0.0;
        for (DetallesIngreso ingreso : detallesIngreso) {
            total += ingreso.calcularImporte();
        }

        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    // Todo refectorizar a enum
    /*
     * 1. Activo
     * 2. Rechazado
     * 3. Pendiente
     * 0. Cancelado
     * 4. Aceptado
     *
     * */

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    /*public List<DetallesIngreso> getDetallesIngreso() {
        return detallesIngreso;
    }*/

    public List<DetallesIngreso> getDetalles() {
        return this.detallesIngreso;
    }

    public void setDetallesIngreso(List<DetallesIngreso> detallesIngreso) {
        this.detallesIngreso = detallesIngreso;
    }

    public void addItemDetalle(DetallesIngreso detallesIngreso) {
        this.detallesIngreso.add(detallesIngreso);
    }

    public String getPersona() {
        return persona.getNombre();
    }

    /*public Persona getPersona() {
        return persona;
    }*/

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public String getUsuario() {
        return usuario.getNombre();
    }

    /* public Usuario getUsuario() {
        return usuario;
    }*/

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

}
