package com.anaya.ventasdemo.repository;

import com.anaya.ventasdemo.model.Usuario;

import java.util.List;

public interface UsuarioRepository {

    Usuario findById(Long id);

    List<Usuario> getUsuarios();

    void store(Usuario usuario);

    void update(Usuario usuario);

    void delete(Long id);

    Usuario login(String email, String password);
}
