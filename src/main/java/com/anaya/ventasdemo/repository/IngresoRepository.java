package com.anaya.ventasdemo.repository;

import com.anaya.ventasdemo.model.DetallesIngreso;
import com.anaya.ventasdemo.model.Ingreso;

import java.util.List;

public interface IngresoRepository {

    Ingreso findById(Long id);

    List<Ingreso> getIngresos();

    void store(Ingreso ingreso);

    void update(Ingreso ingreso);

    void delete(Long id);

    void statusCancel(Long id);

    List<DetallesIngreso> getDetallesIngresos(Long id);
}
