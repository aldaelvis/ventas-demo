package com.anaya.ventasdemo.repository;

import com.anaya.ventasdemo.model.Articulo;

import java.util.List;

public interface ArticuloRepository {

    Articulo findById(Long id);

    List<Articulo> getArticulos();

    void store(Articulo articulo);

    void update(Articulo articulo);

    void delete(Long id);
}
