package com.anaya.ventasdemo.repository;

import com.anaya.ventasdemo.model.Categoria;

import java.util.List;

public interface CategoriaRepository {

    Categoria findById(Long id);

    List<Categoria> getCategorias();

    void store(Categoria categoria);

    void update(Categoria categoria);

    void delete(Long id);


}
