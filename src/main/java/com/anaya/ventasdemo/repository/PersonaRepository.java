package com.anaya.ventasdemo.repository;

import com.anaya.ventasdemo.model.Persona;

import java.util.List;

public interface PersonaRepository {

    Persona findById(Long id);

    List<Persona> getPersonas();

    void store(Persona persona);

    void update(Persona persona);

    void delete(Long id);

}
