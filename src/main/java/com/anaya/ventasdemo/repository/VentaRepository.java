package com.anaya.ventasdemo.repository;

import com.anaya.ventasdemo.model.DetallesVenta;
import com.anaya.ventasdemo.model.Venta;

import java.util.List;
import java.util.Map;

public interface VentaRepository {

    Venta findById(Long id);

    List<Venta> getVentas();

    // List<Map<Long, String>> store(Venta venta);
    Map<Long, String> store(Venta venta);

    void update(Venta venta);

    void delete(Long id);

    void statusCancel(Long id);

    List<DetallesVenta> getDetallesVentas(Long id);
}
