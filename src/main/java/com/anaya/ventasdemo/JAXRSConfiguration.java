package com.anaya.ventasdemo;

import jakarta.annotation.security.DeclareRoles;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.ApplicationPath;
import jakarta.ws.rs.core.Application;
import org.eclipse.microprofile.auth.LoginConfig;


@ApplicationScoped
@ApplicationPath("/api")
@LoginConfig(authMethod = "MP-JWT")
public class JAXRSConfiguration extends Application {

}